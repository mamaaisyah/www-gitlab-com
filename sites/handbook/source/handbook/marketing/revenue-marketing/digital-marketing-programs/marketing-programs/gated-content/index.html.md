---
layout: markdown_page
title: "Gated Content"
description: "Overview of gated content programs at GitLab - this page will be moved under Demand Generation."
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## Overview
Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#overview](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#overview)

#### Types of Content Programs
Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#content-types]()

#### Gating Criteria

No longer applicable. Please see new handbook page.

### Internal Content (created by the GitLab team)

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-internal-content]

#### Organizing content pillar epics and issues

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#epic-issues-internal-content](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#epic-issues-internal-content)


### `Analyst Content` (delivered by analysts)

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-external-content](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-external-content)

#### Organizing analyst content epics and issues

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#epic-issues-external-content](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#epic-issues-external-content)


#### Gating process

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#gated-content-landing-pages](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#gated-content-landing-pages)

##### Requestor

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#process-ungated-content-journeys](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#process-ungated-content-journeys)

##### Campaigns

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#process-ungated-content-journeys](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#process-ungated-content-journeys)

#### Retiring analyst content upon expiration

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#retire-analyst-assets](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#retire-analyst-assets)

##### Retire Marketo landing page

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#retire-marketo-page](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#retire-marketo-page)


### `Competitive Content` (delivered by competitive)

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-external-content](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-external-content)


#### Organizing competitive content epics and issues

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-external-content](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#ungated-external-content)

## Where to upload final asset

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#process-ungated-content-journeys](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#process-ungated-content-journeys)

## Marketo automation and landing page creation

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-landing-pages](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-landing-pages)

#### Create Marketo program, tokens, and SFDC campaign sync

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-mkto-sfdc](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-mkto-sfdc)

#### Edit registration page and thank you page URLs

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-registration-page](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-registration-page)

#### Edit "resulting page" from the form submit

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-resulting-page](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-resulting-page)

#### Activate smart campaign(s)

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-activate-smart-campaigns](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-activate-smart-campaigns)

#### Update SFDC campaign

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-update-sfdc](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-update-sfdc)

#### Test live registration page and flows

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-testing](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#steps-gated-testing)

## Add new content to the Resources page

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#add-to-resources-page](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#add-to-resources-page)

## How to retire analyst assets when they expire

Please reference and bookmark this new location for Content in Campaigns, and change wherever referenced in the handbook: [https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#retire-analyst-assets](https://about.gitlab.com/handbook/marketing/demand-generation/campaigns/content-in-campaigns/#retire-analyst-assets)
